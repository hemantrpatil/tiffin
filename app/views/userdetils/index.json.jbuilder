json.array!(@userdetils) do |userdetil|
  json.extract! userdetil, :id, :fname, :lname, :address, :city, :state, :zipcode, :mobileno, :landlineno
  json.url userdetil_url(userdetil, format: :json)
end
