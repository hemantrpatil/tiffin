# == Schema Information
#
# Table name: products
#
#  id                 :integer          not null, primary key
#  product_name       :string(255)
#  product_type       :string(255)
#  price              :integer
#  info               :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  user_id            :integer
#



class Product < ActiveRecord::Base
	has_attached_file :image, :styles => { :large => "600x600>",:medium => "300x300>", :thumb => "150x150>" }
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
	


	has_many :comments, dependent: :destroy
	belongs_to :user
	before_create :randomize_id



private
def randomize_id
  begin
    self.id = SecureRandom.random_number(1_0_000)
  end 
end
end
