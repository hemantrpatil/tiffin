# == Schema Information
#
# Table name: userdetils
#
#  id         :integer          not null, primary key
#  fname      :string(255)
#  lname      :string(255)
#  address    :string(255)
#  city       :string(255)
#  state      :string(255)
#  zipcode    :string(255)
#  mobileno   :string(255)
#  landlineno :string(255)
#  emailid    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Userdetil < ActiveRecord::Base

	belongs_to :user

	before_create :randomize_id

	private
	def randomize_id
	  begin
	    self.id = SecureRandom.random_number(1_000_000)
	  end 
	end
	
end
