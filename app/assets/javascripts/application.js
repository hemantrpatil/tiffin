// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

//= require bootstrap.min.js
//= require jquery.bxslider.min.js
//= require jquery.easing.min.js
//= require jquery.owl.carousel.min.js
//= require masonry.pkgd.min.js
//= require perfect-scrollbar.min.js
//= require jquery.magnific-popup.min.js
//= require jquery.magnific-popup.min.js
//= require jquery.parallax-1.1.3.js
//= require retina.min.js
//= require jquery.form.min.js
//= require jquery.validate.min.js
//= require scripts.js
//= require jquery.min.js
//= require jquery-1.11.2.min.js
//= require jquery.wow.min.js
//= require analytics.js
//= require scripts.js



