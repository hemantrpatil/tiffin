class UserdetilsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_userdetil, only: [:show, :edit, :update, :destroy]

  # GET /userdetils
  # GET /userdetils.json
  def index
    @userdetils = current_user.userdetil.all
       if Userdetil.where(:user_id => current_user.id).present?
               userdetils_path
       else
             redirect_to new_userdetil_path
       end
  end

  # GET /userdetils/1
  # GET /userdetils/1.json
  def show
  end

  # GET /userdetils/new
  def new
    @userdetil =  Userdetil.new
  end

  # GET /userdetils/1/edit
  def edit
  end

  # POST /userdetils
  # POST /userdetils.json
  def create
    @userdetil = Userdetil.new(userdetil_params)
    @userdetil.user_id = current_user.id
    respond_to do |format|
      if @userdetil.save
         UserNotifier.send_signup_email(@userdetil).deliver_now
        format.html { redirect_to userdetils_path, notice: 'Userdetil was successfully created.' }
        format.json { render :show, status: :created, location: userdetils_path }
      else
        format.html { render :new }
        format.json { render json: @userdetil.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /userdetils/1
  # PATCH/PUT /userdetils/1.json
  def update
    respond_to do |format|
      if @userdetil.update(userdetil_params)
        format.html { redirect_to userdetils_path, notice: 'Userdetil was successfully updated.' }
        format.json { render :show, status: :ok, location: userdetils_path }
      else
        format.html { render :edit }
        format.json { render json: @userdetil.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /userdetils/1
  # DELETE /userdetils/1.json
  def destroy
    @userdetil.destroy
    respond_to do |format|
      format.html { redirect_to userdetils_url, notice: 'Userdetil was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_userdetil
      @userdetil = Userdetil.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def userdetil_params
      params.require(:userdetil).permit(:fname, :lname, :address, :city, :state, :zipcode, :mobileno, :landlineno,:emailid , :user_id)
    end
end
