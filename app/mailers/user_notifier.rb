class UserNotifier < ActionMailer::Base
  default :from => 'hemantvicky.patil@gmail.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(userdetil )
    @userdetil = userdetil
    mail( :to => @userdetil.emailid,
    :subject => 'Tiffin.com, Confirm Your Order' )
  end
end
