require 'test_helper'

class UserdetilsControllerTest < ActionController::TestCase
  setup do
    @userdetil = userdetils(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:userdetils)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create userdetil" do
    assert_difference('Userdetil.count') do
      post :create, userdetil: { address: @userdetil.address, city: @userdetil.city, fname: @userdetil.fname, landlineno: @userdetil.landlineno, lname: @userdetil.lname, mobileno: @userdetil.mobileno, state: @userdetil.state, zipcode: @userdetil.zipcode }
    end

    assert_redirected_to userdetil_path(assigns(:userdetil))
  end

  test "should show userdetil" do
    get :show, id: @userdetil
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @userdetil
    assert_response :success
  end

  test "should update userdetil" do
    patch :update, id: @userdetil, userdetil: { address: @userdetil.address, city: @userdetil.city, fname: @userdetil.fname, landlineno: @userdetil.landlineno, lname: @userdetil.lname, mobileno: @userdetil.mobileno, state: @userdetil.state, zipcode: @userdetil.zipcode }
    assert_redirected_to userdetil_path(assigns(:userdetil))
  end

  test "should destroy userdetil" do
    assert_difference('Userdetil.count', -1) do
      delete :destroy, id: @userdetil
    end

    assert_redirected_to userdetils_path
  end
end
