# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(email: 'vicky@gmail.com', password: 12345678)


products = [{Product id: 4050, product_name: "Product name", product_type: "veg", price: 56, info: "The Non-Vegetarian side of Indian cuisine comprise...", created_at: "2016-01-18 10:47:52", updated_at: "2016-01-18 10:47:52", image_file_name: "anime-hd-wallpapers-widescreen-desktop1.jpg", image_content_type: "image/jpeg", image_file_size: 955732, image_updated_at: "2016-01-18 10:47:51", user_id: 221554}, 
{Product id: 4990, product_name: "Product name", product_type: "non veg", price: 89, info: "The Non-Vegetarian side of Indian cuisine comprise...", created_at: "2016-01-18 10:47:29", updated_at: "2016-01-18 10:47:29", image_file_name: "hd-wallpapers-191.jpg", image_content_type: "image/jpeg", image_file_size: 338695, image_updated_at: "2016-01-18 10:47:28", user_id: 221554}, 
{Product id: 6412, product_name: "Product name", product_type: "non veg", price: 91, info: "The Non-Vegetarian side of Indian cuisine comprise...", created_at: "2016-01-18 10:48:52", updated_at: "2016-01-18 10:48:52", image_file_name: "widescreen-6.jpg", image_content_type: "image/jpeg", image_file_size: 752686, image_updated_at: "2016-01-18 10:48:51", user_id: 22155},
 {Product id: 9098, product_name: "Product name", product_type: "veg", price: 78, info: "The Non-Vegetarian side of Indian cuisine comprise...", created_at: "2016-01-18 10:47:08", updated_at: "2016-01-18 10:47:08", image_file_name: "HD-Wallpapers-CEA.jpg", image_content_type: "image/jpeg", image_file_size: 1465148, image_updated_at: "2016-01-18 10:47:07", user_id: 221554}]
products.each do |product|
	Product.new(product).save
end
