class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :product_name
      t.string :product_type
      t.integer :price
      t.string :info

      t.timestamps null: false
    end
  end
end
