class CreateUserdetils < ActiveRecord::Migration
  def change
    create_table :userdetils do |t|
      t.string :fname
      t.string :lname
      t.string :address
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :mobileno
      t.string :landlineno
      t.string :emailid

      t.timestamps null: false
    end
  end
end
